package ru.lmd.bankaccount;

import ru.lmd.bankaccount.exceptions.NegativeValueException;
import ru.lmd.bankaccount.exceptions.OutOfBalanceValue;

public class Main {

    public static void main(String[] args) throws NegativeValueException, InterruptedException {

        Account account = new Account();

        Thread depositThread = new DepositThread(account, 10);
        depositThread.start();

        try {
            account.withdraw(500);
        } catch (OutOfBalanceValue outOfBalanceValue) {
            System.err.println("Not enough money for withdraw");
        }

        depositThread.join();

        System.out.println("balance: " + account.getBalance());

    }
}
