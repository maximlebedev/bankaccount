package ru.lmd.bankaccount;

import ru.lmd.bankaccount.exceptions.NegativeValueException;

public class DepositThread extends Thread {

    private Account account;
    private long money;

    DepositThread(Account account, long money) {
        this.account = account;
        this.money = money;
    }

    @Override
    public void run() {

        for (int i = 0; i < 10; i++) {

            try {
                account.deposit(money);
            } catch (NegativeValueException e) {
                e.printStackTrace();
            }
        }
    }
}